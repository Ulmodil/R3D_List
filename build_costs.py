def build_costs(btype,start,end):
    costs = {
        'money': 0,
        'gold': 0,
        'oil': 0,
        'ore': 0,
        'diamonds': 0,
        'uranium': 0
    }
    if btype == 1:
        # Hospital
        for i in range(start+1,end+1):
            costs['money']    += round(pow(i*300,1.5))
            costs['gold']     += round(pow(i*2160,1.5))
            costs['oil']      += round(pow(i*160,1.5))
            costs['ore']      += round(pow(i*90,1.5))
            costs['diamonds'] += round(pow(i*0,1.5))
            costs['uranium']  += round(pow(i*0,1))
    elif btype == 2:
        # Military Base
        for i in range(start+1,end+1):
            costs['money']    += round(pow(i*300,1.5))
            costs['gold']     += round(pow(i*2160,1.5))
            costs['oil']      += round(pow(i*160,1.5))
            costs['ore']      += round(pow(i*90,1.5))
            costs['diamonds'] += round(pow(i*0,1.5))
            costs['uranium']  += round(pow(i*0,1))
    elif btype == 3:
        # School
        for i in range(start+1,end+1):
            costs['money']    += round(pow(i*300,1.5))
            costs['gold']     += round(pow(i*2160,1.5))
            costs['oil']      += round(pow(i*160,1.5))
            costs['ore']      += round(pow(i*90,1.5))
            costs['diamonds'] += round(pow(i*0,1.5))
            costs['uranium']  += round(pow(i*0,1))
    elif btype == 4:
        # Missile System
        for i in range(start+1,end+1):
            costs['money']    += round(pow(i*1000,1.5))
            costs['gold']     += round(pow(i*180,1.5))
            costs['oil']      += round(pow(i*10,1.5))
            costs['ore']      += round(pow(i*10,1.5))
            costs['diamonds'] += round(pow(i*10,0.7))
            costs['uranium']  += round(pow(i*0,1))
    elif btype == 5:
        # Sea Port
        for i in range(start+1,end+1):
            costs['money']    += round(pow(i*1000,1.5))
            costs['gold']     += round(pow(i*180,1.5))
            costs['oil']      += round(pow(i*10,1.5))
            costs['ore']      += round(pow(i*10,1.5))
            costs['diamonds'] += round(pow(i*10,0.7))
            costs['uranium']  += round(pow(i*0,1))
    elif btype == 6:
        # Power Plant
        for i in range(start+1,end+1):
            costs['money']    += round(pow(i*2000,1.5))
            costs['gold']     += round(pow(i*90,1.5))
            costs['oil']      += round(pow(i*25,1.5))
            costs['ore']      += round(pow(i*25,1.5))
            costs['diamonds'] += round(pow(i*5,0.7))
            costs['uranium']  += round(pow(i*20,1.5))
    elif btype == 7:
        # Spaceport
        for i in range(start+1,end+1):
            costs['money']    += round(pow(i*6000,1.5))
            costs['gold']     += round(pow(i*180,1.5))
            costs['oil']      += round(pow(i*30,1.5))
            costs['ore']      += round(pow(i*25,1.5))
            costs['diamonds'] += round(pow(i*10,0.7))
            costs['uranium']  += round(pow(i*30,1.5))
    elif btype == 8:
        # Airport
        for i in range(start+1,end+1):
            costs['money']    += round(pow(i*1000,1.5))
            costs['gold']     += round(pow(i*180,1.5))
            costs['oil']      += round(pow(i*10,1.5))
            costs['ore']      += round(pow(i*10,1.5))
            costs['diamonds'] += round(pow(i*10,0.7))
            costs['uranium']  += round(pow(i*0,1))
    elif btype == 9:
        # House Fund
        for i in range(start+1,end+1):
            costs['money']    += round(pow(i*30,1.5))
            costs['gold']     += round(pow(i*216,1.5))
            costs['oil']      += round(pow(i*16,1.5))
            costs['ore']      += round(pow(i*9,1.5))
            costs['diamonds'] += round(pow(i*0,1.5))
            costs['uranium']  += round(pow(i*0,1))
    
    return costs
