#!/usr/bin/env python3

#######################################################################
#  Copyright (C) 2018, Ulmodil Dolphin
#
#  This file is part of R3D List
#
#  R3D List is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  R3D List is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with R3D List; if not, see <http://www.gnu.org/licenses/>.
#######################################################################

import configparser
import requests
from bs4 import BeautifulSoup
import sqlite3
from update_db import get_cookie

# request the war declaration page
# filter the region fill data
# remove those with the default color
# categorize the remaining regions by declaration type

# Selected Region: #3c6ae0
# None: #cccccc
# Land War: #1e6d09
# Sea War: #09296d
# Both: #096d6c

config = configparser.ConfigParser()
config.read('settings.ini')
db_file = config['File Locations']['region database']

def clean(s):
    w_types = {'1e6d09':'land','09296d':'sea','096d6c':'both'}
    region,w_type = s.split(':')
    region = int(region[1:])
    w_type = w_types[ w_type[-7:-1] ]
    return (region,w_type)

def scrape(regions_file):
    url = "http://rivalregions.com/map/region_data_wars/{}"
    ua = config['Web Requests']['user agent string']
    conn = sqlite3.connect(db_file)
    cur = conn.cursor()
    cur.execute("""
        CREATE TABLE IF NOT EXISTS war_declaration (
            region_id INTEGER,
            neighbor INTEGER,
            war_type TEXT,
            PRIMARY KEY (region_id,neighbor)
        )
    """)
    with open(regions_file,'r') as file:
        query = "INSERT OR REPLACE INTO war_declaration VALUES (?,?,?)"
        regions = file.readlines()
        for r in regions:
            r = int( r.strip() )
            req = requests.post(url.format(r), headers={'user-agent':ua}, cookies=get_cookie())
            soup = BeautifulSoup(req.content,'html.parser')
            t = soup.find_all(name='script')[0].text
            t = t[t.find("{")+1:t.find("}")].split(',')
            data = [( r,*clean(x) ) for x in t if 'cccccc' not in x and '3c6ae0' not in x]
            with conn:
                cur.executemany(query,data)

scrape('war_dec_lists/earth-regions.txt')
scrape('war_dec_lists/antarctic-regions.txt')
scrape('war_dec_lists/moon-regions.txt')