#!/usr/bin/env python3

#######################################################################
#  Copyright (C) 2018, Ulmodil Dolphin
#
#  This file is part of R3D List
#
#  R3D List is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  R3D List is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with R3D List; if not, see <http://www.gnu.org/licenses/>.
#######################################################################

import configparser
import sqlite3
import requests
from bs4 import BeautifulSoup
from itertools import zip_longest,repeat
from math import ceil
from re import compile

config = configparser.ConfigParser()
config.read('settings.ini')
db_file = config['File Locations']['region database']
rconn = sqlite3.connect(db_file)
rcur = rconn.cursor()

def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)

def get_cookie():
    # Location of the cookie database in firefox
    cookie_file = config['Web Requests']['cookie database']

    # Connect to database
    con = sqlite3.connect(cookie_file)
    cur = con.cursor()
 
    # The fields we need
    fields = ['PHPSESSID','rr','rr_id','rr_add']
    # Build cookie dict
    cookie = {}
    for f in fields:
        # Database query
        cur.execute(
            "SELECT value " +
            "FROM moz_cookies WHERE name = '{}' ".format(f) +
            "AND ( host = 'rivalregions.com' OR host = '.rivalregions.com' )")
        # Get result and add to cookie dict
        v = cur.fetchall()[0][0]
        cookie[f] = v
    
    return cookie

def parse_state_page(state_id=0):
    # Make request with custom user agent and cookies
    url = "http://rivalregions.com/info/regions/{}".format(state_id)
    ua = config['Web Requests']['user agent string']
    r = requests.post(url, headers={'user-agent':ua}, cookies=get_cookie())
    soup = BeautifulSoup(r.content,'html.parser')
    data = [ d.text for d in soup.findAll('td') ]
    if not(len(data)):
        raise RuntimeError("State ID {} has no regions and may not exist".format(state_id))
    else:
        rows = [ list(r) for r in grouper(data,55) ]
        for r in rows:
            name,id = r.pop(0).split(',\xa0id:\xa0')
            r[0:0] = [id,name]
            r[1] = "\"{}\"".format(r[1])
            r[2] = 1 if r[2] == 'да' else 0
        return rows

def create_master():
# When fetching for sheets, inner join master and state_regions on region_id
# When updating state regions, parse state page and update into master table
    add_region_entry = " INSERT OR REPLACE INTO all_regions VALUES ({}) ".format(','.join('?'*56))
    rows = parse_state_page()
    with rconn:
        rcur.executemany(add_region_entry,rows)

def update_state(state_id):
    delete_regions = "DELETE FROM state_regions WHERE state_id = ?"
    with rconn:
        rcur.execute(delete_regions,(state_id,))

    add_regions = "INSERT OR REPLACE INTO state_regions VALUES (?,?)"
    rows = parse_state_page(state_id=state_id)
    region_ids = ( r[0] for r in rows )
    with rconn:
        rcur.executemany(add_regions,zip(region_ids,repeat(state_id)))

    add_region_entry = " INSERT OR REPLACE INTO all_regions VALUES ({}) ".format(','.join('?'*56))
    with rconn:
        rcur.executemany(add_region_entry,rows)
    update_autonomies(state_id)
    update_budgets(state_id)

def update_state_regions(state_id):
    delete_regions = "DELETE FROM state_regions WHERE state_id = ?"
    with rconn:
        rcur.execute(delete_regions,(state_id,))

    add_regions = "INSERT OR REPLACE INTO state_regions VALUES (?,?)"
    rows = parse_state_page(state_id=state_id)
    region_ids = ( r[0] for r in rows )
    with rconn:
        rcur.executemany(add_regions,zip(region_ids,repeat(state_id)))

    add_region_entry = " INSERT OR REPLACE INTO all_regions VALUES ({}) ".format(','.join('?'*56))
    with rconn:
        rcur.executemany(add_region_entry,rows)

def update_all_state_regions():
    url = "http://rivalregions.com/listed/states"
    ua = config['Web Requests']['user agent string']
    r = requests.post(url, headers={'user-agent':ua}, cookies=get_cookie())
    soup = BeautifulSoup(r.content,'html.parser')
    states = soup.select('tr.header_buttons_hover')
    names = [ x.select_one('td.list_name').string for x in states ]
    states = [ int(x['user']) for x in states ]
    for state in states:
        update_state_regions(state)
    with rconn:
        state_names = zip(states,names)
        rcur.executemany("INSERT OR REPLACE INTO state_names VALUES (?,?)", state_names)

def update_index_req(update_master_table=False):
    if update_master_table:
        create_master()

    rcur.execute("""
        SELECT h,m,s,d
        FROM (
            SELECT min(hospitals) h, health_index hi
            FROM all_regions WHERE hi < 11 GROUP BY hi
        )
        INNER JOIN (
            SELECT min(military_bases) m, military_index mi
            FROM all_regions WHERE mi < 11 GROUP BY mi
        )
        ON hi = mi
        INNER JOIN (
            SELECT min(schools) s, education_index ei
            FROM all_regions WHERE ei < 11 GROUP BY ei
        )
        ON mi = ei
        INNER JOIN (
            SELECT min(houses) d, development_index di
            FROM all_regions WHERE di < 11 GROUP BY di
        )
        ON ei = di
        ORDER BY hi ASC;
    """)
    mins = rcur.fetchall()
    bufs = [ [ ceil(v*1.02) for v in row ] for row in mins ]

    def flat_zip(x,y):
        return [ item for sublist in zip(x,y) for item in sublist ]

    reqs = [ [i]+flat_zip(m,b) for i,m,b in zip(range(1,12),mins,bufs) ]
    
    with rconn:
        rcur.executemany("INSERT OR REPLACE INTO index_req VALUES (?,?,?,?,?,?,?,?,?)", reqs)

def update_autonomies(state_id,cookie=get_cookie()):
    with rconn:
        rcur.execute("""
            DELETE FROM autonomies
            WHERE region_id
            IN (
                SELECT region_id
                FROM state_regions
                WHERE state_id = ?
            )
        """,(state_id,))

    # autonomy_id is either:
    #   state_id for non-autonomies,
    #   region_id for single autonomies,
    #   or big_autonomy_id

    ua = config['Web Requests']['user agent string']
    url = "http://rivalregions.com/map/state_details/{}".format(state_id)
    r = requests.get(url, headers={'user-agent':ua}, cookies=get_cookie())
    soup = BeautifulSoup(r.content,'html.parser')
    state_name = soup.select_one('a.dot').string

    with rconn:
        rcur.execute("""
            INSERT INTO autonomies
            SELECT a.region_id,sr.state_id, ?
            FROM all_regions a
            INNER JOIN state_regions sr
            ON a.region_id = sr.region_id
            WHERE sr.state_id = ? and autonomy = 0
        """,(state_name,state_id,))

    url = "http://rivalregions.com/listed/bigautonomies/{}".format(state_id)
    r = requests.get(url, headers={'user-agent':ua}, cookies=get_cookie())
    soup = BeautifulSoup(r.content,'html.parser')

    # map/details/+ for single autonomies
    # map/autonomy_details/+ for big autonomies
    auts = soup.findAll(class_='list_name pointer',action=compile('map'))
    is_big_aut = compile('map/autonomy_details/+')
    for a in auts:
        aname = a.string
        atype = a['action']
        aut_id = atype.split('/')[-1]
        if is_big_aut.match(atype):
            aut_url = 'http://rivalregions.com/{}'.format(atype)
            aut_r = requests.get(aut_url, headers={'user-agent':ua}, cookies=get_cookie())
            aut_soup = BeautifulSoup(aut_r.content,'html.parser')
            regions = aut_soup.findAll(action=compile('map/details'))
            aut_ids = [ r['action'].split('/')[-1] for r in regions ]
            for r_id in aut_ids:
                with rconn:
                    rcur.execute("INSERT OR REPLACE INTO autonomies VALUES (?,?,?)",(r_id,aut_id,aname))
        else:
            with rconn:
                rcur.execute("INSERT OR REPLACE INTO autonomies VALUES (?,?,?)",(aut_id,aut_id,aname))

def update_budgets(state_id,cookie=get_cookie()):
    with rconn:
        rcur.execute("""
            DELETE FROM budgets
            WHERE autonomy_id
            IN (
                SELECT autonomy_id
                FROM autonomies a
                INNER JOIN state_regions s
                ON a.region_id = s.region_id
                WHERE state_id = ?
                GROUP BY autonomy_id
            )
        """,(state_id,))
    rcur.execute("""
        SELECT a.region_id,autonomy_id aid
        FROM autonomies a
        INNER JOIN state_regions s
        ON a.region_id = s.region_id
        WHERE state_id = ?
        AND aid != ?
        GROUP BY aid
    """,(state_id,state_id))
    auts = rcur.fetchall()
    auts += [[state_id,state_id]]
    single_url = 'http://rivalregions.com/map/details/{}'
    big_url = 'http://rivalregions.com/map/autonomy_details/{}'
    state_url = 'http://rivalregions.com/map/state_details/{}'
    ua = config['Web Requests']['user agent string']
    for a in auts:
        if a[0] == state_id:
            r = requests.get(state_url.format(a[1]),headers={'user-agent':ua},cookies=get_cookie())
        elif a[0] == a[1]:
            r = requests.get(single_url.format(a[1]),headers={'user-agent':ua},cookies=get_cookie())
        else:
            r = requests.get(big_url.format(a[1]),headers={'user-agent':ua},cookies=get_cookie())
        soup = BeautifulSoup(r.content,'html.parser')
        money = soup.select_one('div.imp > span.oil').previous_sibling.previous_sibling.previous_sibling.previous_sibling.string
        gold = soup.select_one('div.imp > span.oil').previous_sibling.previous_sibling.string
        oil = soup.select_one('div.imp > span.oil').string
        ore = soup.select_one('div.imp > span.ore').string
        uranium = soup.select_one('div.imp > span.uranium').string
        diamond = soup.select_one('div.imp > span.diamond').string
        fmt = lambda x: int( x.split('\xa0')[0].replace('.','') )
        with rconn:
            rcur.execute(
                'INSERT OR REPLACE INTO budgets VALUES (?,?,?,?,?,?,?)',
                (a[1],fmt(money),fmt(gold),fmt(oil),fmt(ore),fmt(uranium),fmt(diamond))
            )
