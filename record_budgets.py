#!/usr/bin/env python3

#######################################################################
#  Copyright (C) 2018, Ulmodil Dolphin
#
#  This file is part of R3D List
#
#  R3D List is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  R3D List is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with R3D List; if not, see <http://www.gnu.org/licenses/>.
#######################################################################

import update_db
from query_db import get_budgets
from datetime import datetime,timezone,timedelta

est = timezone(-timedelta(hours=4),'EST')

def record_budgets(state_id,record_file):
    update_db.update_state(state_id)
    update_db.update_autonomies(state_id)
    update_db.update_budgets(state_id)
    stamp = datetime.now(est).strftime("%H:%M %Z %d/%m/%y")
    with open(record_file,'a') as file:
        file.write(stamp)
        file.write('\n')
        for x in get_budgets(state_id):
            file.write(','.join(map(str,x)))
            file.write('\n')
