#!/usr/bin/env python3

#######################################################################
#  Copyright (C) 2018, Ulmodil Dolphin
#
#  This file is part of R3D List
#
#  R3D List is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  R3D List is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with R3D List; if not, see <http://www.gnu.org/licenses/>.
#######################################################################

import configparser
from os.path import join
import xlsxwriter
import query_db
from itertools import count
from datetime import datetime,timezone,timedelta

config = configparser.ConfigParser()
config.read('settings.ini')
sheet_dir = config['File Locations']['sheets directory']

def update_state_sheet(state_id):
    file = join(sheet_dir,"state_{}.xlsx".format(state_id))
    book = xlsxwriter.Workbook(file)

    font_format = book.add_format({'font_name': 'Arial','font_size':10})
    integer_format = book.add_format({'font_name': 'Arial','font_size':10,'num_format':'#,##0'})
    grey_format = book.add_format({'font_color': '#CCCCCC','bg_color': '#CCCCCC'})
    green_format = book.add_format({'bg_color': '#B7E1CD'})
    yellow_format = book.add_format({'bg_color': '#FCE8B2'})
    red_format = book.add_format({'bg_color': '#F4C7C3'})

    indexes_sheet = book.add_worksheet('Indexes')
    indexes_sheet.conditional_format('C2:F1000000', {
        'type': 'cell',
        'criteria': 'between',
        'minimum': 1,
        'maximum': 3,
        'format': red_format
    })
    indexes_sheet.conditional_format('C2:F1000000', {
        'type': 'cell',
        'criteria': 'between',
        'minimum': 4,
        'maximum': 6,
        'format': yellow_format
    })
    indexes_sheet.conditional_format('C2:F1000000', {
        'type': 'cell',
        'criteria': '>=',
        'value': 7,
        'format': green_format
    })
    est = timezone(-timedelta(hours=4),'EST')
    stamp = datetime.now(est).strftime("%H:%M %Z %d/%m/%y")
    offr = 0
    for r,row in zip(count(), query_db.get_indexes(state_id)):
        indexes_sheet.write_row(r,0,row,font_format)
        offr += 1
    for c,col in zip(count(), query_db.get_health_numbers(state_id)):
        indexes_sheet.write_column(0,c+6,col,font_format)
    for r,row in zip(count(),query_db.desired_indexes(state_id)):
        indexes_sheet.write_row(r,8,row,font_format)
    indexes_sheet.write(offr+1,0,"Updated "+stamp,font_format)

    builds_sheet = book.add_worksheet('Buildings')
    offr = 0
    for row in query_db.get_buildings(state_id):
        builds_sheet.write_row(offr,0,row,font_format)
        offr += 1
    for c,col in zip(count(),query_db.get_state_build_stats(state_id)):
        builds_sheet.write_column(offr+1,c+1,col,font_format)

    ineeded_sheet = book.add_worksheet('IndexReq')
    ineeded_sheet.conditional_format('D2:K1000000', {
        'type': 'cell',
        'criteria': '<',
        'value': 0,
        'format': grey_format
    })
    for r,row in zip(count(), query_db.get_index_needed(state_id)):
        ineeded_sheet.write_row(r,0,row,font_format)

    budget_sheet = book.add_worksheet('Budgets')
    budget_sheet.set_column(1,11,16)
    for c,col in zip(count(),query_db.get_budgets(state_id)):
        budget_sheet.write(0,c,col[0],font_format)
        budget_sheet.write_column(1,c,col[1:],integer_format)
    offr = 9
    for r,row in zip(count(),query_db.get_region_building_costs(state_id,9)):
        offr += 1
        if isinstance(row,list):
            budget_sheet.write(r+9,0,row.pop(0),font_format)
            for c,cell in zip(count(),row):
                budget_sheet.write_formula(r+9,c+1,cell,integer_format)
        else:
            budget_sheet.write_row(r+9,0,row,font_format)
    budget_sheet.write(offr,0,'Total Costs',font_format)
    offr += 1
    for c,col in zip(count(),query_db.get_total_costs(state_id)):
        budget_sheet.write(offr,c,col[0],font_format)
        budget_sheet.write_column(offr+1,c,col[1:],integer_format)
    budget_sheet.write(offr+9,0,'Needed')
    for c,col in zip(count(),query_db.get_resources_needed(state_id,offr)):
        budget_sheet.write(offr+10,c,col[0],font_format)
        budget_sheet.write_column(offr+11,c,col[1:],integer_format)

    power_sheet   = book.add_worksheet('Power')
    power_sheet.conditional_format('C2:C1000000', {
        'type': 'cell',
        'criteria': '<',
        'value': 0,
        'format': red_format
    })
    power_sheet.conditional_format('D2:D1000000', {
        'type': 'cell',
        'criteria': '>',
        'value': 0,
        'format': red_format
    })
    power_sheet.conditional_format('E2:E1000000', {
        'type': 'cell',
        'criteria': '>',
        'value': 0,
        'format': green_format,
        'multi_range': 'C2:C1000000 E2:E1000000'
    })
    offr = 0
    for r,row in zip(count(), query_db.get_power_numbers(state_id)):
        power_sheet.write_row(r,0,row,font_format)
        offr += 1
    for r,row in zip(count(), query_db.get_state_power_numbers(state_id)):
        power_sheet.write_row(offr+1,2,row,font_format)
        offr += 1

    war_nums_sheet = book.add_worksheet('War')
    offr = 0
    fmt_start = 0
    fmt_options = {
        'type': 'cell',
        'criteria': '>',
        'value': '',
        'format': red_format
    }
    column_nums = ['C','F','I','L','O','R']
    for row in query_db.get_war_numbers(state_id):
        if row[0] == 'Initial Attack':
            fmt_start = offr
        elif not row[0]:
            for cn in column_nums:
                fmt_options['value'] = '{}{}'.format(cn,fmt_start)
                for fr in range(fmt_start+1,offr+1):
                    war_nums_sheet.conditional_format('{}{}'.format(cn,fr),fmt_options)
        war_nums_sheet.write(offr,0,row[0],font_format)
        war_nums_sheet.write_row(offr,1,row[1:],integer_format)
        offr += 1

    book.close()
