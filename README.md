# R3D List

Fetches region data from Rival Regions and processes it for a spreadsheet.

## Getting Started

### Prerequisites

__Disclaimer:__ _R3D List has currently only been tested on Linux with Firefox._

* Firefox (any recent version should work)
* `git`
* `sqlite3`
* `python3`
* `pip3`

To install these on a Debian-like system:

```shell
apt install firefox-esr git sqlite3 python3 python3-pip
```

You will need a few Python packages installed. These are in `requirements.txt` and you can install them using:

```shell
pip3 install -r requirements.txt
```

### Setting Up R3D List

First off, you will need to clone the project:

```shell
git clone https://gitlab.com/Ulmodil/R3D_List.git
```

In the newly created directory, you need to make sure that `init.py` and `main.py` are executable:

```shell
chmod +x init.py main.py
```

After that, you need to run `init.py` to create `settings.ini`:

```shell
./init.py
```

After `settings.ini` has been created, you will need to edit a few settings.  

#### Necessary Settings

The two necessary values are under the section `[Web Requests]`.  
To find these values, you can go to `about:support` in Firefox.
Copy the `User Agent` value from there into `settings.ini`, i.e.:

```
[Web Requests]
user agent string = Mozilla/5.0 (etc.) etc.
```

To set `cookie database`, find `Profile Directory` in `about:support` and click `Open Directory`.
From there, you will need to find `cookies.sqlite` and copy its filepath into `settings.ini`, i.e.:

```
[Web Requests]
cookie database = /home/username/path/to/cookies.sqlite
```

#### Optional Settings

Optionally, you can set where you would like to keep the region database if you desire to move it, and set where the spreadsheets will be created.

```
[File Locations]
region database = /wherever/you/moved/it.sqlite
sheets directory = /path/to/folder/for/sheets
```

## Using R3D List

### Cookies

R3D List works by parsing the page `rivalregions.com/info/regions` and its subpages. To do this however, R3D List needs to use the cookies created by playing RR, so first you must log in to RR to create the cookies. The cookies should last for a while, but you may need to refresh them after a few days.

### Creating and Updating Tables

The second step in using R3D List involves updating the region database. To start off, it is recommended that you create the master region table:

```shell
./main.py -U
```

This may take a few minutes as there are many regions to process. You should do this semi-frequently to keep the table up-to-date.

To create the state tables that will be used to create the spreadsheets, you must provide the state id number, which is the number at the end of the url when you click on a state in RR, i.e.:

```
rivalregions.com/#state/details/2366
```

And to create the table:

```shell
./main.py -u -id 2366
```

Finally, to create the spreadsheet for the state:

```shell
./main.py -s -id 2366
```

You can also string these options together to do multiple things at once:

```shell
./main.py -U -u -s -id 2366
```

And that's it! The spreadsheet will be created for your viewing and analyzing pleasure!

## Attribution

__Beautiful Soup 4__  
Project code: https://code.launchpad.net/~leonardr/beautifulsoup/bs4  
Copyright (c) 2004-2016 Leonard Richardson  
License: (MIT) https://bazaar.launchpad.net/~leonardr/beautifulsoup/bs4/view/head:/LICENSE

__XlsxWriter__  
Project Code: https://github.com/jmcnamara/XlsxWriter  
Copyright (c) 2013, John McNamara <jmcnamara@cpan.org>  
License: (BSD) https://github.com/jmcnamara/XlsxWriter/blob/master/LICENSE.txt
