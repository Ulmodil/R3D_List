#!/usr/bin/env python3

#######################################################################
#  Copyright (C) 2018, Ulmodil Dolphin
#
#  This file is part of R3D List
#
#  R3D List is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  R3D List is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with R3D List; if not, see <http://www.gnu.org/licenses/>.
#######################################################################

import configparser
from sqlite3 import connect,OperationalError

config = configparser.ConfigParser()

try:
    # Read config and make connection
    import update_db
    # Create master table
    with update_db.rconn:
        with open('schema.sqlite') as schema:
            update_db.rcur.executescript(schema.read())
    # Fetch all regions
    update_db.create_master()
    update_db.update_index_req()
except KeyError:
    config['File Locations'] = {
        'region database': 'r3d.sqlite',
        'sheets directory': ''
    }
    config['Web Requests'] = {
        'cookie database': '',
        'user agent string': ''
    }

    with open('settings.ini','w') as configfile:
        config.write(configfile)
    
    print('settings.ini created')
    print('Please set Web Requests values')
except OperationalError:
    print('Please set Web Requests values')
