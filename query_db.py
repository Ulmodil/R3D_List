#!/usr/bin/env python3

#######################################################################
#  Copyright (C) 2018, Ulmodil Dolphin
#
#  This file is part of R3D List
#
#  R3D List is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  R3D List is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with R3D List; if not, see <http://www.gnu.org/licenses/>.
#######################################################################

import configparser
import sqlite3
import update_db
from math import ceil, floor
from itertools import count
from xlsxwriter.utility import xl_col_to_name

con = update_db.rconn
cur = update_db.rcur

def get_index_needed(state_id):
    query = """
        SELECT all_regions.region_id, english_name, index_level,
        (health_index_min - hospitals),
        (health_index_2_percent - hospitals),
        (military_index_min - military_bases),
        (military_index_2_percent - military_bases),
        (education_index_min - schools),
        (education_index_2_percent - schools),
        (development_index_min - houses),
        (development_index_2_percent - houses)
        FROM all_regions, index_req 
        INNER JOIN region_name_translation 
        ON all_regions.region_id = region_name_translation.region_id 
        INNER JOIN state_regions
        ON all_regions.region_id = state_regions.region_id
        WHERE state_regions.state_id = ?
        ORDER BY all_regions.region_id ASC, index_level DESC
    """

    yield (
        "Region ID", "Region Name", "Index Level",
        "Health", "Health 2% Buffer",
        "Military", "Military 2% Buffer",
        "Education", "Education 2% Buffer",
        "Development", "Development 2% Buffer"
    )
    update_db.update_index_req()
    cur.execute(query, (state_id,))
    for r in cur.fetchall():
        yield r

def get_indexes(state_id):
    yield ("Region ID", "Region Name", "Health Index", "Military Index", "Education Index", "Development Index")
    cur.execute("""
        SELECT all_regions.region_id, english_name, 
        health_index, military_index, 
        education_index, development_index 
        FROM all_regions 
        INNER JOIN region_name_translation 
        ON all_regions.region_id = region_name_translation.region_id 
        INNER JOIN state_regions
        ON all_regions.region_id = state_regions.region_id
        WHERE state_regions.state_id = ?
    """, (state_id,))
    for r in cur.fetchall():
        yield r

def desired_indexes(state_id):
    cur.execute("SELECT count(region_id) FROM state_regions WHERE state_id = ?",(state_id,))
    c = cur.fetchone()[0]
    yield ("Desired Indexes","Health","Military","Education","Development")
    for x in range(0,c):
        yield ('',0,0,0,0)

def get_health_numbers(state_id):
    cur.execute("""
        SELECT health_index FROM all_regions
        INNER JOIN state_regions
        ON all_regions.region_id = state_regions.region_id
        WHERE state_regions.state_id = ?
    """, (state_id,))
    hi = ( h[0] for h in cur.fetchall() )
    def regen_case(h):
        if h < 6:
            return 7
        elif h < 11:
            return h+2
        else:
            return 16
    regen = [ regen_case(h) for h in hi ]
    yield ( "Energy Regen", *regen )
    yield ( "Gold Earned / Day", *[ (r/10)*6*24 for r in regen ] )

def get_power_numbers(state_id):
    yield ("Region ID", "Region Name", "Power Output", "Powerplants Needed", "Buildings to Limit")
    cur.execute("""
        SELECT all_regions.region_id, english_name, 
               ((10 * power_plants) - 
                (2 * (hospitals + military_bases + 
                      schools + missile_systems + 
                      sea_ports + spaceports + airports))) 
        FROM all_regions
        INNER JOIN region_name_translation 
        ON all_regions.region_id = region_name_translation.region_id
        INNER JOIN state_regions
        ON all_regions.region_id = state_regions.region_id
        WHERE state_regions.state_id = ?
    """, (state_id,))
    pp_needed = lambda p_out: 0 if p_out >= 0 else ceil(abs(p_out)/10)
    build_limit = lambda p_out: 0 if p_out <= 0 else floor(p_out/2)
    output = ( (r[0],r[1],r[2],pp_needed(r[2]),build_limit(r[2])) for r in cur.fetchall() )
    for o in output:
        yield o

def get_state_power_numbers(state_id):
    yield ("State Power Output", "State Powerplants Needed", "State Buildings to Limit")
    cur.execute("""
        SELECT sum(
               ((10 * power_plants) - 
                (2 * (hospitals + military_bases + 
                      schools + missile_systems + 
                      sea_ports + spaceports + airports))) 
        )
        FROM all_regions
        INNER JOIN state_regions
        ON all_regions.region_id = state_regions.region_id
        WHERE state_regions.state_id = ?
    """, (state_id,))
    pp_needed = lambda p_out: 0 if p_out >= 0 else ceil(abs(p_out)/10)
    build_limit = lambda p_out: 0 if p_out <= 0 else floor(p_out/2)
    output = cur.fetchone()[0]
    yield (output,pp_needed(output),build_limit(output))

def get_buildings(state_id):
    yield (
        "Region ID", "Region Name",
        "Hospitals", "Military Bases", "Schools",
        "Missile Systems", "Sea Ports", "Powerplants",
        "Space Ports", "Airports", "Houses"
    )
    cur.execute("""
        SELECT all_regions.region_id, english_name,
               hospitals, military_bases, schools,
               missile_systems, sea_ports, power_plants,
               spaceports, airports, houses 
        FROM all_regions
        INNER JOIN region_name_translation 
        ON all_regions.region_id = region_name_translation.region_id
        INNER JOIN state_regions
        ON all_regions.region_id = state_regions.region_id
        WHERE state_regions.state_id = ?
    """, (state_id,))
    for r in cur.fetchall():
        yield r

def get_state_build_stats(state_id):
    yield (
        "State Average",
        "Lowest Region","Lowest Number","Average Without","Average Difference",
        "Highest Region","Highest Number","Average Without","Average Difference"
    )
    buildings = [
        "hospitals", "military_bases", "schools",
        "missile_systems", "sea_ports", "power_plants",
        "spaceports", "airports", "houses" 
    ]
    min_max_query = """
        SELECT english_name,{0}({1})
        FROM all_regions
        INNER JOIN region_name_translation
        ON all_regions.region_id = region_name_translation.region_id
        INNER JOIN state_regions
        ON all_regions.region_id = state_regions.region_id
        WHERE state_regions.state_id = ?
        AND {1} > 0
    """
    avg_query = """
        SELECT sum({0}),count({0})
        FROM all_regions
        INNER JOIN state_regions
        ON all_regions.region_id = state_regions.region_id
        WHERE state_regions.state_id = ?
        AND {0} > 0
    """
    for b in buildings:
        cur.execute(avg_query.format(b),(state_id,))
        s,c = cur.fetchone()
        avg = 0 if c == 0 else s/c
        avg_f = "{:.3f}".format(avg)
        cur.execute(min_max_query.format('min',b),(state_id,))
        mn = cur.fetchone()
        avg_wo_min = 0 if c < 2 else (s-mn[1])/(c-1)
        avg_wo_min_f = "{:.3f}".format(avg_wo_min)
        avg_diff_min = "{:.3f}".format( avg_wo_min - avg )
        cur.execute(min_max_query.format('max',b),(state_id,))
        mx = cur.fetchone()
        avg_wo_max = 0 if c < 2 else (s-mx[1])/(c-1)
        avg_wo_max_f = "{:.3f}".format(avg_wo_max)
        avg_diff_max = "{:.3f}".format( avg - avg_wo_max )
        yield (avg_f,*mn,avg_wo_min_f,avg_diff_min,*mx,avg_wo_max_f,avg_diff_max)

def get_budgets(state_id):
    yield ('ID','Autonomy','Money','Gold','Oil','Ore','Uranium','Diamonds')
    cur.execute("""
        SELECT a.autonomy_id,autonomy_name,money,gold,oil,ore,uranium,diamonds
        FROM budgets b
        INNER JOIN autonomies a
        ON b.autonomy_id = a.autonomy_id
        INNER JOIN state_regions s
        ON a.region_id = s.region_id
        WHERE s.state_id = ?
        GROUP BY a.autonomy_id
    """,(state_id,))
    budgets = cur.fetchall()
    # Move state to front
    budgets[0:0] = [budgets.pop(budgets.index(next(filter(lambda x: x[0]==state_id,budgets))))]
    for b in budgets:
        yield b

def get_region_building_costs(state_id,offset_r):
    query = """
        SELECT english_name,autonomy_id
        FROM autonomies a
        INNER JOIN state_regions s
        ON a.region_id = s.region_id
        INNER JOIN region_name_translation t
        ON a.region_id = t.region_id
        WHERE state_id = ?
    """
    formula_values = {
        1: ('B',{
            'money': (300,1.5),
            'gold': (2160,1.5),
            'oil': (160,1.5),
            'ore': (90,1.5),
            'uranium': (0,1),
            'diamonds': (0,1.5)
        }),
        2: ('C',{
            'money': (300,1.5),
            'gold': (2160,1.5),
            'oil': (160,1.5),
            'ore': (90,1.5),
            'uranium': (0,1),
            'diamonds': (0,1.5)
        }),
        3: ('D',{
            'money': (300,1.5),
            'gold': (2160,1.5),
            'oil': (160,1.5),
            'ore': (90,1.5),
            'uranium': (0,1),
            'diamonds': (0,1.5)
        }),
        4: ('E',{
            'money': (1000,1.5),
            'gold': (180,1.5),
            'oil': (10,1.5),
            'ore': (10,1.5),
            'uranium': (0,1),
            'diamonds': (10,0.7)
        }),
        5: ('F',{
            'money': (1000,1.5),
            'gold': (180,1.5),
            'oil': (10,1.5),
            'ore': (10,1.5),
            'uranium': (0,1),
            'diamonds': (10,0.7)
        }),
        6: ('G',{
            'money': (2000,1.5),
            'gold': (90,1.5),
            'oil': (25,1.5),
            'ore': (25,1.5),
            'uranium': (20,1.5),
            'diamonds': (5,0.7)
        }),
        7: ('H',{
            'money': (6000,1.5),
            'gold': (180,1.5),
            'oil': (30,1.5),
            'ore': (25,1.5),
            'uranium': (30,1.5),
            'diamonds': (10,0.7)
        }),
        8: ('I',{
            'money': (1000,1.5),
            'gold': (180,1.5),
            'oil': (10,1.5),
            'ore': (10,1.5),
            'uranium': (0,1),
            'diamonds': (10,0.7)
        }),
        9: ('J',{
            'money': (30,1.5),
            'gold': (216,1.5),
            'oil': (16,1.5),
            'ore': (9,1.5),
            'uranium': (0,1),
            'diamonds': (0,1.5)
        })
    }
    formula = """
        =ARRAY_CONSTRAIN(
            ARRAYFORMULA(
                ROUND(
                    SUM(
                        ({4}*
                        IF(
                            IF(
                                OR(
                                    AND(
                                        {3}<>1,
                                        {3}<>2,
                                        {3}<>3,
                                        {3}<>9
                                    ),
                                    AND(
                                        {1}=0,
                                        INDEX(
                                            Indexes!J1:S1000000,
                                            MATCH(
                                                {0},
                                                Indexes!B1:B1000000,
                                                0
                                            ),
                                            SWITCH(
                                                {3},
                                                1,1,
                                                2,2,
                                                3,3,
                                                9,4,
                                                10
                                            )
                                        )=0
                                    )
                                ),
                                0,
                                INDEX(
                                    IndexReq!D1:K1000000,
                                    MATCH(
                                        {0},
                                        IndexReq!B1:B1000000,
                                        0
                                    )+10-
                                    IF(
                                        {1}<>0,
                                        {1},
                                        INDEX(
                                            Indexes!J1:S1000000,
                                            MATCH(
                                                {0},
                                                Indexes!B1:B1000000,
                                                0
                                            ),
                                            SWITCH(
                                                {3},
                                                1,1,
                                                2,2,
                                                3,3,
                                                9,4,
                                                10
                                            )
                                        )
                                    ),
                                    SWITCH(
                                        {3},
                                        1,1,
                                        2,3,
                                        3,5,
                                        9,7
                                    )
                                )
                            )+{2}<=0,
                            INDEX(
                                Buildings!C2:K1000000,
                                MATCH(
                                    {0},
                                    Buildings!B2:B1000000,
                                    0
                                ),
                                {3}
                            ),
                            ROW(
                                INDIRECT(
                                    IF(
                                        INDEX(
                                            Buildings!C2:K1000000,
                                            MATCH(
                                                {0},
                                                Buildings!B2:B1000000,
                                                0
                                            ),
                                            {3}
                                        )=0,
                                        1,
                                        INDEX(
                                            Buildings!C2:K1000000,
                                            MATCH(
                                                {0},
                                                Buildings!B2:B1000000,
                                                0
                                            ),
                                            {3}
                                        )
                                    )
                                    &":"&
                                    INDEX(
                                        Buildings!C2:K1000000,
                                        MATCH(
                                            {0},
                                            Buildings!B2:B1000000,
                                            0
                                        ),
                                        {3}
                                    )+
                                    IF(
                                        OR(
                                            AND(
                                                {3}<>1,
                                                {3}<>2,
                                                {3}<>3,
                                                {3}<>9
                                            ),
                                            AND(
                                                {1}=0,
                                                INDEX(
                                                    Indexes!J1:S1000000,
                                                    MATCH(
                                                        {0},
                                                        Indexes!B1:B1000000,
                                                        0
                                                    ),
                                                    SWITCH(
                                                        {3},
                                                        1,1,
                                                        2,2,
                                                        3,3,
                                                        9,4,
                                                        10
                                                    )
                                                )=0
                                            )
                                        ),
                                        0,
                                        INDEX(
                                            IndexReq!D1:K1000000,
                                            MATCH(
                                                {0},
                                                IndexReq!B1:B1000000,
                                                0
                                            )+10-
                                            IF(
                                                {1}<>0,
                                                {1},
                                                INDEX(
                                                    Indexes!J1:S1000000,
                                                    MATCH(
                                                        {0},
                                                        Indexes!B1:B1000000,
                                                        0
                                                    ),
                                                    SWITCH(
                                                        {3},
                                                        1,1,
                                                        2,2,
                                                        3,3,
                                                        9,4,
                                                        10
                                                    )
                                                )
                                            ),
                                            SWITCH(
                                                {3},
                                                1,1,
                                                2,3,
                                                3,5,
                                                9,7
                                            )
                                        )
                                    )+{2}
                                )
                            )
                        )
                        )^{5}
                    ),
                    0
                )-ROUND(
                    ({4}*(
                            INDEX(
                                Buildings!C2:K1000000,
                                MATCH(
                                    {0},
                                    Buildings!B2:B1000000,
                                    0
                                ),
                                {3}
                            )
                    ))^{5}
                )
            ),1,1
        )
    """
    costs_formula_s = """
        =IFERROR(
            IF(
                B{0}={1},
                SUM(B{3}:J{3}),
                SUM(
                    FILTER(
                        B{3}:J{3},
                        B{2}:J{2}<>"A"
                    )
                )
            ),0
        )
    """
    costs_formula_a = """
        =IFERROR(
            IF(
                B{0}={1},
                0,
                SUM(
                    FILTER(
                        B{3}:J{3},
                        B{2}:J{2}="A"
                    )
                )
            ),0
        )
    """
    # 1+offset_r,state_id,1+offset_r+4,res_off
    resources = ('money','gold','oil','ore','uranium','diamonds')
    cur.execute(query,(state_id,))
    for r in cur.fetchall():
        # For each region
        yield r
        yield(
            "Building Type",
            "Hospitals", "Military Bases", "Schools",
            "Missile Systems", "Sea Ports", "Power Plants",
            "Spaceports", "Airports", "House Fund",
            "State Costs", "Autonomy Costs"
        )
        yield ('Index Level',)
        yield ('Building Number',)
        yield ('Budget (S/A)',)
        region_name_cell = 'A{}'.format(1+offset_r)
        res_off = 1+offset_r+5
        for rtype in resources:
            row = [rtype.capitalize()]
            for btype in range(1,10):
                values = formula_values[btype]
                index_level_cell = '{}{}'.format(values[0],1+offset_r+2)
                building_number_cell = '{}{}'.format(values[0],1+offset_r+3)
                k = values[1][rtype][0]
                p = values[1][rtype][1]
                f = formula.format(region_name_cell,index_level_cell,building_number_cell,btype,k,p)
                row.append("".join(f.split()))
            fs = costs_formula_s.format(1+offset_r,state_id,1+offset_r+4,res_off)
            fa = costs_formula_a.format(1+offset_r,state_id,1+offset_r+4,res_off)
            row.append("".join(fs.split()))
            row.append("".join(fa.split()))
            res_off += 1
            yield row
        yield ('',)
        offset_r += 12

def get_total_costs(state_id):
    yield ('ID','Autonomy','Money','Gold','Oil','Ore','Uranium','Diamonds')
    cur.execute("""
        SELECT autonomy_id,autonomy_name
        FROM autonomies a
        INNER JOIN state_regions s
        ON a.region_id = s.region_id
        WHERE state_id = ?
        GROUP BY autonomy_id
    """,(state_id,))
    auts = cur.fetchall()
    auts.insert(0,auts.pop(auts.index(next(filter(lambda x: x[0]==state_id,auts)))))
    cur.execute("SELECT count(region_id) FROM state_regions WHERE state_id = ?",(state_id,))
    num_regions = cur.fetchone()[0]
    rows = [ [ '{}'+str(12*x+r) for x in range(num_regions) ] for r in range(15,21) ]
    state_c = [ [ x.format('K') for x in r ] for r in rows ]
    column = list(auts.pop(0))
    for s in state_c:
        column.append("=SUM({})".format(','.join(s)))
    yield column
    auts_c = [ [ x.format('L') for x in r ] for r in rows ]
    aut_ids = [ 'B'+str(12*x+10) for x in range(num_regions) ]
    for a in auts:
        column = list(a)
        for ac in auts_c:
            _range = "{0}{2}{1}".format('{','}',';'.join(ac))
            _filter = "{0}{2}{1} = {3}".format('{','}',';'.join(aut_ids), a[0])
            column.append("=SUM(FILTER({},{}))".format(_range,_filter))
        yield column

def get_resources_needed(state_id,offset_r):
    yield ('ID','Autonomy','Money','Gold','Oil','Ore','Uranium','Diamonds')
    cur.execute("""
        SELECT autonomy_id,autonomy_name
        FROM autonomies a
        INNER JOIN state_regions s
        ON a.region_id = s.region_id
        WHERE state_id = ?
        GROUP BY autonomy_id
    """,(state_id,))
    auts = cur.fetchall()
    auts.insert(0,auts.pop(auts.index(next(filter(lambda x: x[0]==state_id,auts)))))
    formula = '=IF({1}-{0} < 0,0,{1}-{0})'
    rows = [ formula.format('{0}'+str(3+r),'{0}'+str(offset_r+3+r)) for r in range(6) ]
    for a,c in zip(auts,count(1)):
        column = list(a)
        r = [ x.format(xl_col_to_name(c)) for x in rows ]
        column += r
        yield column

def get_war_numbers(state_id):
    region_query = """
        SELECT sr.region_id, english_name,
        initial_attack, initial_defense, military_bases,
        missile_systems, airports, sea_ports
        FROM state_regions sr
        INNER JOIN region_name_translation t
        ON sr.region_id = t.region_id
        INNER JOIN all_regions ar
        ON sr.region_id = ar.region_id
        WHERE state_id = ?
    """
    numbers = [
        "initial_attack",
        "initial_defense",
        "military_bases",
        "missile_systems",
        "airports",
        "sea_ports"
    ]
    # number, region_id, war_type, number
    query = """
        SELECT sn.name, english_name, MAX({0})
        FROM all_regions ar
        INNER JOIN state_regions sr
        ON ar.region_id = sr.region_id
        INNER JOIN war_declaration w
        ON w.neighbor = sr.region_id
        INNER JOIN state_names sn
        ON sr.state_id = sn.state_id
        INNER JOIN region_name_translation t
        ON sr.region_id = t.region_id
        WHERE w.region_id = ? AND (w.war_type = '{1}' OR w.war_type = 'both')
        GROUP BY sr.state_id
        ORDER BY MAX({0}) desc
    """
    def fetch(d):
        cur.execute(query.format(d[0],d[2]),(d[1],))
        return cur.fetchall()
    def flatten_row(r):
        r1 = []
        for x in r:
            r1 += x
        return r1
    cur.execute(region_query,(state_id,))
    regions = cur.fetchall()
    for r in regions:
        yield r[:2]
        data = [ (n,r[0],'land') for n in numbers[:-1] ]
        land_data = [ fetch(d) for d in data ]
        land_data_rows = list( zip(*land_data) )
        if land_data_rows:
            yield ('Land Wars',*flatten_row([ ('','',n) for n in r[2:-1] ])[1:])
            yield (
                'Initial Attack','','','Initial Defense','','','Military Bases','','',
                'Missile Systems','','','Airports','','',
            )
            for ldr in land_data_rows:
                yield flatten_row(ldr)
            yield ('',)

        data = [ (n,r[0],'sea') for n in numbers ]
        sea_data = [ fetch(d) for d in data ]
        sea_data_rows = list( zip(*sea_data) )
        if sea_data_rows:
            yield ('Sea Wars',*flatten_row([ ('','',n) for n in r[2:] ])[1:])
            yield (
                'Initial Attack','','','Initial Defense','','','Military Bases','','',
                'Missile Systems','','','Airports','','','Sea Ports','',''
            )
            for sdr in sea_data_rows:
                yield flatten_row(sdr)
            yield ('',)
