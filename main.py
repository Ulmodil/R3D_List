#!/usr/bin/env python3

#######################################################################
#  Copyright (C) 2018, Ulmodil Dolphin
#
#  This file is part of R3D List
#
#  R3D List is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  R3D List is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with R3D List; if not, see <http://www.gnu.org/licenses/>.
#######################################################################

import update_db
import query_db
import argparse
import sheets
from record_budgets import record_budgets

parser = argparse.ArgumentParser(description="Update and query region numbers")
parser.add_argument("-id","--state_id", type=int)
parser.add_argument("-u","--update", action="store_true")
parser.add_argument("-U","--update-all-regions", action="store_true")
parser.add_argument("-q","--query", nargs='*', choices=['indexes','ineeded','power'])
parser.add_argument("-s","--sheet", action="store_true")
parser.add_argument("-r","--record-budgets", nargs=2)
func_map = {
    'indexes': query_db.get_indexes,
    'ineeded': query_db.get_index_needed,
    'power': query_db.get_power_numbers
}

args = parser.parse_args()
if args.update_all_regions:
    update_db.create_master()
    update_db.update_all_state_regions()
if args.update:
    update_db.update_state(args.state_id)
if args.sheet:
    sheets.update_state_sheet(args.state_id)
if args.query:
    for q in args.query:
        func_map[q](args.state_id)
if args.record_budgets:
    record_budgets(int(args.record_budgets[0]),args.record_budgets[1])
